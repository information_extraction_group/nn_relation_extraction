# -*- coding: utf-8 -*-

import os, json, string, json, re, numpy as np , sys
from tqdm import tqdm
from gensim.models import Word2Vec as w2v
from gensim.models.word2vec import PathLineSentences

with open('../../model_params.json') as f:
    model_params = json.load(f)

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
RESOURCES_FOLDER = os.path.join(module_location, '../../resources/')

unknown          = "_UNKNOWN"
PUNCTUATION      = '"#$%&\'()*,/=@[\\]^`{|}~'

def cleanLine(line):
    """Clean a line for further processing

    Delete predefined punctuations

    Args:
        param1 (str): line with words and stuff
    
    Returns:
        str: cleaned line

    """
    words = line.split()
    table = str.maketrans('', '', PUNCTUATION)
    stripped = [w.translate(table) for w in words if w != '']
    line = ' '.join(stripped)
    return line

def sem_tasksToTxt(paths):
    """Convert XML to plain text

    All tags are deleted, the result is saved in a given path.
    This function is needed for the semEval task

    Args:
        param1 (list): A list with pairs of lists, [[source1, dest1], [source2, dest2], ...]

    """
    import xml.etree.ElementTree as ET
    for path in paths:
        tree = ET.parse(path[0])
        root = tree.getroot()
        text = ''.join(root.itertext())
        text = re.sub('\n+','\n',text)
        with open(path[1], 'w', encoding='utf8') as fOut:
            fOut.write(text)

def bio_tasksToTxt(files):
    """Get all text files from BioNLP and concatenate them to a new file

    All filenames from source are read,
    Only .txt files are concatenated and saved to dest.

    Args:
        param1 (list): [source, dest]
    
    """

    import locale
    for data in files:
        source = data[0]
        dest   = data[1]
        
        names = set([])
        for filename in os.listdir(source):
            if 'LICENSE' not in filename and 'README' not in filename:
                names.add('.'.join(filename.split('.')[:-1]))
        
        with open(dest, 'w', encoding='utf-8') as fOut:
            for name in names:
                with open(source + name + ".txt", "r", encoding='utf-8') as fIn:
                    for line in fIn:
                        fOut.write(line)

def removeTags(line):
    """ Remove e-tags from a line

    Args:
        param1 (str) line to change

    Returns:
        str: cleaned line

    """
    return line.replace('<e1>', '').replace('</e1>', '').replace('<e2>', '').replace('</e2>', '')

def sem2010_test_toTxt(path):
    """Clean lines of the semEval test corpus
    
    Args:
        param1 (str): path to the semEval-2010 test corpus

    Returns:
        list: cleaned lines
    
    """
    cleanLines = []
    with open(path, 'r') as fIn:
        for line in fIn:
            line = line.split('\t')[1]
            line = line.replace('\n', '').strip('"')
            line = removeTags(line)
            cleanLines.append(line)
    return cleanLines

def sem2010_train_toTxt(path):
    """Clean lines of the semEval train corpus
    
    Args:
        param1 (str): path to the semEval-2010 train corpus

    Returns:
        list: cleaned lines
    
    """
    cleanLines = []
    with open(path, 'r') as fIn:
        for line in fIn:
            try:
                int(line.split('\t')[0])
                line = line.split('\t')[1].replace('\n', '').strip('"')
                line = removeTags(line)
                cleanLines.append(line)
            except ValueError:
                continue 
    return cleanLines


def clean_files(source_dir, dest_dir):
    """Create all cleaned corpus files from a directory

    This function is specifically used for ACE:
    Go through all raw corpus files,
    clean those and save them in a seperate directory.

    Args:
        param1 (str): source directory
        param2 (str): destination directory

    Returns:
        int: amount of tokens found

    """
    directory = os.fsencode(source_dir)
    count_tokens = 0
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        with open(source_dir + filename, 'r', encoding='utf-8') as fIn:
            with open(dest_dir + filename, 'w', encoding='utf-8') as fOut:
                for line in fIn:
                    count_tokens += len(line.split())
                    line = cleanLine(line)
                    fOut.write(line + '\n')
    return count_tokens

if __name__ == '__main__':
    import argparse

    ####################################################################################################
    # Load Args Parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', choices=['clean', 'train'])
    parser.add_argument('--domain', default='semEval', choices=['semEval', 'semEval2010', 'BioNLP', 'scienceIE'])
    parser.add_argument('--raw', '-r', action='count')

    args = parser.parse_args()
    mode = args.mode
    domain = args.domain

    ####################################################################################################
    # Clean corpus
    if(mode == 'clean'):
        source = RESOURCES_FOLDER + domain + '/embeddings/raw/'
        dest   = RESOURCES_FOLDER + domain + '/embeddings/clean/'

        if domain == 'semEval':
            source = RESOURCES_FOLDER + domain + '/embeddings/ACL_raw/'
            dest   = RESOURCES_FOLDER + domain + '/embeddings/ACL_clean/'
            files = [[RESOURCES_FOLDER + domain + '/raw/1.1.text.xml', RESOURCES_FOLDER + domain + '/embeddings/ACL_raw/1.1.text.txt'],
                    [RESOURCES_FOLDER + domain + '/raw/1.2.text.xml', RESOURCES_FOLDER + domain + '/embeddings/ACL_raw/1.2.text.txt']]
            sem_tasksToTxt(files)

        elif domain == 'BioNLP':
            if not os.path.exists(RESOURCES_FOLDER + domain + '/embeddings/raw/'):
                os.makedirs(RESOURCES_FOLDER + domain + '/embeddings/raw/')
            files = [[RESOURCES_FOLDER + domain + '/raw/BioNLP-ST_2011_Entity_Relations_development_data/', RESOURCES_FOLDER + domain + '/embeddings/raw/development.txt'],
                    [RESOURCES_FOLDER + domain + '/raw/BioNLP-ST_2011_Entity_Relations_training_data/', RESOURCES_FOLDER + domain + '/embeddings/raw/training.txt']]
            bio_tasksToTxt(files)

        elif domain == 'scienceIE':
            # 74.342 Tokens
            if not os.path.exists(RESOURCES_FOLDER + domain + '/embeddings/raw/'):
                os.makedirs(RESOURCES_FOLDER + domain + '/embeddings/raw/')
            files = [[RESOURCES_FOLDER + domain + '/raw/scienceie2017_train/train2/', RESOURCES_FOLDER + domain + '/embeddings/raw/train.txt'],
                    [RESOURCES_FOLDER + domain + '/raw/semeval_articles_test/', RESOURCES_FOLDER + domain + '/embeddings/raw/test.txt']]
            bio_tasksToTxt(files)
        
        elif domain == 'semEval2010':
            # 184.466 Tokens
            # Vocab Size: 30.049
            if not os.path.exists(RESOURCES_FOLDER + domain + '/embeddings/raw/'):
                os.makedirs(RESOURCES_FOLDER + domain + '/embeddings/raw/')

            testSentences = sem2010_test_toTxt(RESOURCES_FOLDER + domain + '/raw/SemEval2010_task8_testing/TEST_FILE.txt')
            trainSentences = sem2010_train_toTxt(RESOURCES_FOLDER + domain + '/raw/SemEval2010_task8_training/TRAIN_FILE.TXT')
            
            trainCorpus = RESOURCES_FOLDER + domain + '/embeddings/raw/training.txt'
            testCorpus = RESOURCES_FOLDER + domain + '/embeddings/raw/testing.txt'
            with open(testCorpus, 'w') as fOut:
                for line in testSentences:
                    fOut.write(line + '\n')
            with open(trainCorpus, 'w') as fOut:
                for line in trainSentences:
                    fOut.write(line + '\n')

        if not os.path.exists(dest):
            os.makedirs(dest)
        
        count = clean_files(source, dest)
        print('> Token amount in raw data: {}'.format(count))
        print('> Saved to: \"{}\"'.format(dest))

    ####################################################################################################
    # train model
    if(mode == 'train'):
        save_dir = os.path.join(RESOURCES_FOLDER, domain + '/embeddings/')
        if not os.path.exists(save_dir):
                os.makedirs(save_dir)

        if(args.raw and args.raw > 0 and domain == 'semEval'):
            if domain == 'semEval':
                source = RESOURCES_FOLDER + domain + '/embeddings/ACL_raw/'
            else:
                source = RESOURCES_FOLDER + domain + '/embeddings/raw/'
        else:
            if domain == 'semEval':
                source = RESOURCES_FOLDER + domain + '/embeddings/ACL_clean/'
            else:
                source = RESOURCES_FOLDER + domain + '/embeddings/clean/'
           
        dest      = RESOURCES_FOLDER + domain + '/embeddings/word_emb.txt'
        sentences = PathLineSentences(source)

        model      = w2v(sentences, size=model_params['w2v_emb_size'], min_count=1)
        vocab_size = len(model.wv.vocab)
        vocab      = model.wv.vocab
        print('Vocabulary size: {}'.format(vocab_size))

        with open(dest, 'w', encoding='utf8') as fOut:
                for w in tqdm(vocab):
                    embedding = model.wv[w]
                    embedding = ' '.join(str(e) for e in embedding)
                    fOut.write(w + ' ' + embedding + '\n')