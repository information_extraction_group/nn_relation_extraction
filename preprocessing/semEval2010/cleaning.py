PUNCTUATION = '"#$%&\'()*,/=@[\\]^`{|}~'

def cleanLine(line):
    """Remove punctuation and reset . ! and ?

    Args:
        param1 (str): line that must be cleaned

    Returns:
        str: cleaned line
    
    """
    words = line.split()
    table = str.maketrans('', '', PUNCTUATION)
    stripped = [w.translate(table).strip() for w in words if w != '']
    line = ' '.join(stripped)
    return line.replace(" .", ".").replace(" ?", "?").replace(" !", "!").strip()

def clean(read, write):
    """Clean everything in the input file
    
    Args:
        param1 (str): Path to the input file
        param2 (str): Path to the destination file
        
    """

    with open(read, 'r', encoding='utf-8') as fIn:
        with open(write, 'w', encoding='utf-8') as fOut:
            nextIsRelation = False
            sentence = ''
            for line in fIn:
                line = line.replace('\n', '')

                if nextIsRelation:
                    nextIsRelation = False
                    relation = line.split('(', 1)[0]

                    rType = ''
                    if relation != 'Other':
                        type = line.split('(', 1)[1].strip(')')
                        if type.find('e1') < type.find('e2'):
                            rType = '_NORMAL'
                        else:
                            rType = '_REVERSED'
                    else:
                        relation = 'none'
                    fOut.write(relation.upper() + rType + ';;' + cleanLine(sentence) + '\n')
                    continue
                try:
                    sample = int(line.split('\t')[0])
                    print("Sample: {}".format(sample))
                    nextIsRelation = True

                    # save text here
                    sentence = line.split('\t')[1].strip('"')
                    leftCut = '<e> ' + sentence.split('<e1>', 1)[1]
                    rightCut = leftCut.split('</e2>', 1)[0] + ' <e> '
                    sentence = rightCut.replace('</e1>', ' <e> ').replace('<e2>', ' <e> ')
                    sentence = ' '.join(sentence.split())
                except ValueError:
                    nextIsRelation = False
                    
                