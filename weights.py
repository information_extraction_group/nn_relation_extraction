import ast, operator, numpy as np
from core import io, labels as myLabels

RESOURCES_FOLDER = 'resources/'
META_SPLIT = ';;'

def count_labels(data, labels):
    """Count number of all labels found in the given data

    Args:
        param1 (list): lines with relations
        param2 (list): all relevant labels

    Returns:
        dict: labels with numbers they were found

    """
    labelCount = {}
    for label in labels:
        labelCount[label] = 0

    for line in data:
        label = '_'.join(line.split(META_SPLIT)[:-1])
        if label:
            labelCount[label] += 1
    return labelCount

def get_cross_entropy_weights(data, labels):
    """Calculate weights for every label

    Count every label, the weight is antiproportional to the number a label was found.

    Args:
        param1 (list): lines with relations
        param2 (list): all relevant labels

    Returns:
        list: weights for each label in the same order as param2

    """
    weights = []

    labelCount = count_labels(data, labels)

    for label in list(labels.keys()):
        weights.append(labelCount[label])

    sumW = sum(weights)
    print("Amount of different classes in corpus: {}".format(weights))
    N = len(labels)

    weights = [sumW/(N * w) if N * w != 0 else -1 for w in weights]
    return weights

def w_rnn(sentence, min_sent_len, max_sent_len):
    """Calculate the influence of the LSTM as percent

    Args:
        param1 (str): input sentence
        param2 (int): min sentence length in training data
        param3 (int): max sentence length in training data
    
    Returns:
        float: LSTM influence

    """
    si = getSi(sentence, min_sent_len, max_sent_len)
    return 0.5 + (np.sign(si) * (si ** 2))

def getSi(sentence, min_sent_len, max_sent_len):
    sent_len = get_sent_len(sentence)
    return ((sent_len - min_sent_len) / (max_sent_len - min_sent_len)) - 0.5
    
def get_sent_len(sentence):
    """get sentence length if the sentence is a list of words ids

    When a number is zero, the sentence is at its end
    >>> print(get_sent_len([1, 43, 67, 32, 0, 0, 0, 0]))
    4

    Args:
        param1 (list): input sentence with indices

    Returns:
        int: length of sentence

    """
    return sum([1 for i in sentence if i > 0])

# Test functions
if __name__ == '__main__':
    train_path = RESOURCES_FOLDER + 'semEval2010/preprocessed/cleaning.training.txt'
    train_data = io.load_data(train_path, keepBreaks=False)
    label2idx = getattr(myLabels, "label2idx_semEval10")
    sorted_x = sorted(count_labels(train_data, label2idx).items(), key=operator.itemgetter(1))
    print(sorted_x)    