# -*- coding: utf-8 -*-
# A special module for SemEval-2018 Task 2

import cleaning1

META_SPLIT      = ';;'
max_line_length = 19

PUNCTUATION = '"#$%&\'()*,/=@[\\]^`{|}~'

def removeInnerTags(line):
    """If there are entities between a pair, delete their tags

    Args:
        param1 (str): input line
        >>> print(removeInnerTags('<e>entitiy1<e> ... <e>entitiy2<e> ... <e>entity3<e>'))
        '<e>entitiy1<e> ... entitiy2 ... <e>entity3<e>'

    Returns:
        str: line without inner <e>-tags
    
    """
    types = line.split(META_SPLIT)[:-1]
    line  = line.split(META_SPLIT)[-1]
    text  = ''

    splits = line.split('<e>')

    for i, t in enumerate(splits):
        t = t.strip()
        if i == 1:
            text += '<e> ' + t + ' <e> '
        elif i == (len(splits)-2):
            text += '<e> ' + t + ' <e>'
        else:
            text += t + ' '
    text = text.strip()
    types.append(text)
    return META_SPLIT.join(types)

def tooLong(line):
    """Test if the given line is too long
    
    Args:
        param1 (str): line to check length
        
    Returns:
        boolean: is it too long or not?
        
    """
    middle = line.split('<e>')[2]
    middle = middle.strip()
    if len(middle.split()) > max_line_length:
        return True
    else:
        return False

def clean(read, write):
    """Remove sentences, that are too long and inner tags for task 2
    
    Args:
        param1 (str): source file path
        param2 (str): destination path
    
    """
    with open(read, 'r', encoding='utf-8') as fIn:
        with open(write, 'w', encoding='utf-8') as fOut:
            for line in fIn:
                if line.strip() == '':
                    fOut.write('\n')
                    continue
                line = line.replace('\n', '')
                line = cleaning1.cleanLine(line)
                line = removeInnerTags(line)
                
                if not tooLong(line):
                    text = line.split(';;')[-1]

                    eTags = []
                    for n, w in enumerate(text.split()):
                        if w == '<e>':
                            eTags.append(n)

                    # Check if each line has exactly 2 related entities (4 times the <e> tag) #
                    assert(len(eTags) == 4)

                    # Check that the distance is small enough #
                    assert(eTags[2] - eTags[1] - 1 <= max_line_length)

                    if(len(line.split(';;')) > 2):
                        s = line.split(';;')
                        myType = '_'.join(s[:-1])
                        line = myType + ';;' + s[-1]
                    
                    line = ' '.join(line.split())
                    fOut.write(line + '\n')