import sys

def unify(seq, idfun=None): 
   # order preserving
   if idfun is None:
       def idfun(x): return x
   seen = {}
   result = []
   for item in seq:
       marker = idfun(item)
       # in old Python versions:
       # if seen.has_key(marker)
       # but in new ones:
       if marker in seen: continue
       seen[marker] = 1
       result.append(item)
   return result

def orderNodes(nodes, edges):
    """Order Nodes by amount of edges they appear in

    Nodes with many relations are at the beginning

    Args:
        param1 (list): nodes as entities for all relations in a sentence
        param2 (list): all pairs of connected nodes

    Returns:
        list: sorted nodes by amount of edges
    """

    import operator
    dict = {}
    for node in nodes:
        for edge in edges:
            if node in edge[0] or node in edge[1]:
                if node in dict:
                    dict[node] += 1
                else:
                    dict[node] = 1
    sorted_nodes = sorted(dict.items(), key=operator.itemgetter(1))
    sorted_nodes = list(reversed([key[0] for key in sorted_nodes]))
    return sorted_nodes

def mostRelations(nodes, edges):
    """Use Backtracking to find a graph, that has most relations 
    
    The graph must fulfills the constraint, that a max(edges(n_i)) = 1 for all i

    Args:
        param1 (list): nodes as entities for all relations in a sentence
        param2 (list): all pairs of connected nodes

    Returns:
        list: all IDs of relations that must be deleted, to fulfill the constraint

    """
    if len(edges) == 1:
        return []

    for edge in edges:
        edge[5] = False
    configuration = [False for e in edges]
    relations = 0
    
    bestConfiguration = getMostRelations(nodes, edges, configuration, relations)

    deleteRelationsIds = []
    for edge, conf in zip(edges, bestConfiguration):
        if not conf:
            deleteRelationsIds.append(edge[3])
    return deleteRelationsIds

def getMostRelations(nodes, edges, configuration, bestRelations):
    """Function used for Backtracking

    Args:
        param1 (list): nodes as entities for all relations in a sentence
        param2 (list): all pairs of connected nodes
        param3 (list): booleans that indicate for each relation, if it will be deleted
        param4 (int): saves number of configuration with most relations

    Returns:
        list: final configuration with booleans

    """
    for edge in edges:
        if not edge[5]:
            edge[5] = True

            # test if new configuration is legit #
            if correctGraph(nodes, edges):
                rels = len([e for e in edges if e[5]])
                if rels > bestRelations:
                    configuration = [w[5] for w in edges]
                    bestRelations = rels
                if rels == bestRelations:
                    newImportance = sum([w[2] / w[6] for w in edges if w[5]])
                    oldImportance = sum([w[2] / w[6] for i, w in enumerate(edges) if configuration[i]])
                    if newImportance > oldImportance:
                        configuration = [w[5] for w in edges]
                        bestRelations = rels
            else:
                edge[5] = False
                continue

            configuration = getMostRelations(nodes, edges, configuration, bestRelations)
            edge[5] = False
    return configuration

def deleteRelations(nodes, edges, useImportance=True):
    """Naive way to fulfill the constraint of max edges = 1

    Delete relations until the constraint is fulfilled

    Args:
        param1 (list): nodes as entities for all relations in a sentence
        param2 (list): all pairs of connected nodes
        useImportance (boolean, optional): if true, importance is used to decide what edge is kept, otherwise only distance is used

    Returns:
        list: IDs of relations that will be deleted

    """
    deleteRelationsIds = set([])
    # go through nodes, begin with the one that has the most relations #
    for node in nodes:
        neighbours = []
        for edge in edges:
            if (node in edge[0] or node in edge[1]):
                # check if edge was already deleted #
                edge[5] and neighbours.append(edge)

        # if this node has too many edges, only keep the most important one #
        if len(neighbours) > 1:
            maxImportance = 0
            bestEdge = []
            for n in neighbours:
                importance = n[6] / n[2] if useImportance else n[6]
                if importance > maxImportance:
                    maxImportance = importance
                    bestEdge = n

            for edge in edges:
                if (edge[5] and node in edge[0] or node in edge[1]):
                    # node is in this edge! #
                    if edge != bestEdge:
                        edge[5] = False

        for edge in edges:
            if not edge[5]:
                deleteRelationsIds.add(edge[3])
    return deleteRelationsIds

def correctGraph(nodes, edges):
    """Check if the graph has only nodes with a maximum of 1 edge

    Go through all nodes and count edges they are part of,
    If a node is part of more than one edge, return False.

    Args:
        param1 (list): nodes as entities for all relations in a sentence
        param2 (list): all pairs of connected nodes

    Returns:
        boolean: condition fulfilled?

    """
    for node in nodes:
        inEdges = 0
        for edge in edges:
            if edge[5] and (node in edge[0] or node in edge[1]):
                inEdges += 1
        if inEdges > 1:
            return False
    return True

def getGraph(predictedSentence, predictions, probabilities):
    """ get graph structure for a sentence

    Args:
        param1 (list): relations for this sentence
        param2 (list): labels for each relation
        param3 (list): softmax probability for the best label

    Returns:
        list, list: nodes and edges for the sentence
    """

    edges    = []
    position = 0
    nodes    = []
    for line, prediction, prob in zip(predictedSentence, predictions, probabilities):
        # The NONE class is not relevant, so skip it #
        if prediction != 'NONE':
            line = line.split(';;')[-1]
            word_sections = line.split('|')
            positions = []

            # get all token positions for <e> tags in this line #
            for i, word_section in enumerate(word_sections):
                if '<e>' in word_section.split(',')[0]:
                    positions.append(i)
            
            # Since we iterated over the sentence, the <e> tags are ordered! #
            left_sections  = word_sections[positions[0]+1:(positions[1])] # <e> entity_1 <e>
            right_sections = word_sections[positions[2]+1:(positions[3])] # <e> entity_2 <e>

            # create some node values: <e>_word1_word2_<e>#
            leftNode = '<e>_'
            rightNode = '<e>_'
            for l in left_sections:
                leftNode += l.split(',')[0] + '_' 
            for r in right_sections:
                rightNode += r.split(',')[0] + '_'

            leftNode += '<e>'
            rightNode += '<e>'

            # add nodes to list #
            nodes.append(leftNode)
            nodes.append(rightNode)

            # get edge with distance and probability #
            distance  = (positions[2] - positions[1]) - 1

            edge = [leftNode, rightNode, distance, position, prediction, True, prob]
            edges.append(edge)
        position += 1
    nodes = unify(nodes)
    nodes = orderNodes(nodes, edges)
    return nodes, edges

def postProcessing(predict_data_set, allPredictions, predictedProbabilities):
    """Fullfill the constraint: each node can only be part of no ore one edge

    Args:
        param1 (list): prediction data, can have form [[P1, P2, ..., Pn], [P1, P2, ..., Pm], ...] or [P1, P2, ..., Pn]
        param2 (list): all predictions made for param1
        param3 (list): like param2, however the softmax probability is saved
    
    Returns:
        list: prostprocessed preditions with the same format as param2
    
    """
    newAllPredictions = []
    for predictedSentence, predictions, probabilities in zip(predict_data_set, allPredictions, predictedProbabilities):
        nodes, edges = getGraph(predictedSentence, predictions, probabilities)
        
        # mostRelations(nodes, edges) -> Backtracking
        # mostRelations(nodes, edges) -> use Importance
        # deleteRelations(nodes, edges, useImportance=False) -> use Distance
        deleteIds = deleteRelations(nodes, edges, useImportance = False) 

        for id in deleteIds:
            predictions[id] = 'NONE'

        # check new graph
        nodes, edges = getGraph(predictedSentence, predictions, probabilities)
        for node in nodes:
            relations = 0
            for edge in edges:
                if node in edge[0] or node in edge[1]:
                    relations += 1
            if relations > 1:
                print("There is something wrong...")

        newAllPredictions.append(predictions)

    return newAllPredictions

# test a manually labeled document
if __name__ == "__main__":
    predictedSentence = ['MODEL-FEATURE_REVERSED;;<e>,EEE,0,-5|documents,NNS,0,-4|<e>,EEE,0,-3|according,VBG,1,-2|to,TO,2,-1|<e>,EEE,3,0|name,NN,4,0|references,NNS,5,0|<e>,EEE,6,0',
     'NONE;;<e>,EEE,0,-11|documents,NNS,0,-10|<e>,EEE,0,-9|according,VBG,1,-8|to,TO,2,-7|name,VB,3,-6|references,NNS,4,-5|can,MD,5,-4|be,VB,6,-3|useful,JJ,7,-2|for,IN,8,-1|<e>,EEE,9,0|information,NN,10,0|retrieval,NN,11,0|<e>,EEE,12,0',
     'NONE;;<e>,EEE,0,-16|documents,NNS,0,-15|<e>,EEE,0,-14|according,VBG,1,-13|to,TO,2,-12|name,VB,3,-11|references,NNS,4,-10|can,MD,5,-9|be,VB,6,-8|useful,JJ,7,-7|for,IN,8,-6|information,NN,9,-5|retrieval,NN,10,-4|or,CC,11,-3|as,IN,12,-2|a,DT,13,-1|<e>,EEE,14,0|preprocessor,NN,15,0|<e>,EEE,16,0',
     'NONE;;<e>,EEE,0,-19|documents,NNS,0,-18|<e>,EEE,0,-17|according,VBG,1,-16|to,TO,2,-15|name,VB,3,-14|references,NNS,4,-13|can,MD,5,-12|be,VB,6,-11|useful,JJ,7,-10|for,IN,8,-9|information,NN,9,-8|retrieval,NN,10,-7|or,CC,11,-6|as,IN,12,-5|a,DT,13,-4|preprocessor,NN,14,-3|for,IN,15,-2|more,JJR,16,-1|<e>,EEE,17,0|knowledge,NN,18,0|intensive,JJ,19,0|tasks,NNS,20,0|<e>,EEE,21,0',
     'NONE;;<e>,EEE,0,-8|name,NN,0,-7|references,NNS,0,-6|<e>,EEE,0,-5|can,MD,1,-4|be,VB,2,-3|useful,JJ,3,-2|for,IN,4,-1|<e>,EEE,5,0|information,NN,6,0|retrieval,NN,7,0|<e>,EEE,8,0',
     'NONE;;<e>,EEE,0,-13|name,NN,0,-12|references,NNS,0,-11|<e>,EEE,0,-10|can,MD,1,-9|be,VB,2,-8|useful,JJ,3,-7|for,IN,4,-6|information,NN,5,-5|retrieval,NN,6,-4|or,CC,7,-3|as,IN,8,-2|a,DT,9,-1|<e>,EEE,10,0|preprocessor,NN,11,0|<e>,EEE,12,0',
     'NONE;;<e>,EEE,0,-16|name,NN,0,-15|references,NNS,0,-14|<e>,EEE,0,-13|can,MD,1,-12|be,VB,2,-11|useful,JJ,3,-10|for,IN,4,-9|information,NN,5,-8|retrieval,NN,6,-7|or,CC,7,-6|as,IN,8,-5|a,DT,9,-4|preprocessor,NN,10,-3|for,IN,11,-2|more,JJR,12,-1|<e>,EEE,13,0|knowledge,NN,14,0|intensive,JJ,15,0|tasks,NNS,16,0|<e>,EEE,17,0',
     'NONE;;<e>,EEE,0,-21|name,NN,0,-20|references,NNS,0,-19|<e>,EEE,0,-18|can,MD,1,-17|be,VB,2,-16|useful,JJ,3,-15|for,IN,4,-14|information,NN,5,-13|retrieval,NN,6,-12|or,CC,7,-11|as,IN,8,-10|a,DT,9,-9|preprocessor,NN,10,-8|for,IN,11,-7|more,JJR,12,-6|knowledge,NN,13,-5|intensive,JJ,14,-4|tasks,NNS,15,-3|such,JJ,16,-2|as,IN,17,-1|<e>,EEE,18,0|database,NN,19,0|extraction,NN,20,0|<e>,EEE,21,0',
     'NONE;;<e>,EEE,0,-7|information,NN,0,-6|retrieval,NN,0,-5|<e>,EEE,0,-4|or,CC,1,-3|as,IN,2,-2|a,DT,3,-1|<e>,EEE,4,0|preprocessor,NN,5,0|<e>,EEE,6,0',
     'NONE;;<e>,EEE,0,-10|information,NN,0,-9|retrieval,NN,0,-8|<e>,EEE,0,-7|or,CC,1,-6|as,IN,2,-5|a,DT,3,-4|preprocessor,NN,4,-3|for,IN,5,-2|more,JJR,6,-1|<e>,EEE,7,0|knowledge,NN,8,0|intensive,JJ,9,0|tasks,NNS,10,0|<e>,EEE,11,0',
     'NONE;;<e>,EEE,0,-15|information,NN,0,-14|retrieval,NN,0,-13|<e>,EEE,0,-12|or,CC,1,-11|as,IN,2,-10|a,DT,3,-9|preprocessor,NN,4,-8|for,IN,5,-7|more,JJR,6,-6|knowledge,NN,7,-5|intensive,JJ,8,-4|tasks,NNS,9,-3|such,JJ,10,-2|as,IN,11,-1|<e>,EEE,12,0|database,NN,13,0|extraction,NN,14,0|<e>,EEE,15,0',
     'NONE;;<e>,EEE,0,-5|preprocessor,NN,0,-4|<e>,EEE,0,-3|for,IN,1,-2|more,JJR,2,-1|<e>,EEE,3,0|knowledge,NN,4,0|intensive,JJ,5,0|tasks,NNS,6,0|<e>,EEE,7,0',
     'NONE;;<e>,EEE,0,-10|preprocessor,NN,0,-9|<e>,EEE,0,-8|for,IN,1,-7|more,JJR,2,-6|knowledge,NN,3,-5|intensive,JJ,4,-4|tasks,NNS,5,-3|such,JJ,6,-2|as,IN,7,-1|<e>,EEE,8,0|database,NN,9,0|extraction,NN,10,0|<e>,EEE,11,0',
     'NONE;;<e>,EEE,0,-7|knowledge,NN,0,-6|intensive,JJ,0,-5|tasks,NNS,0,-4|<e>,EEE,0,-3|such,JJ,1,-2|as,IN,2,-1|<e>,EEE,3,0|database,NN,4,0|extraction,NN,5,0|<e>,EEE,6,0']
    
    precitions = ['USAGE_REVERSED',
                  'PART_WHOLE_NORMAL',
                  'RESULT_NORMAL',
                  'RESULT_NORMAL',
                  'RESULT_NORMAL',
                  'RESULT_NORMAL',
                  'RESULT_NORMAL',
                  'RESULT_NORMAL',
                  'USAGE_REVERSED',
                  'RESULT_NORMAL',
                  'RESULT_NORMAL',
                  'PART_WHOLE_NORMAL',
                  'RESULT_NORMAL',
                  'USAGE_REVERSED']

    probabilities = [0.93,
                     0.83,
                     0.74,
                     0.67,
                     0.45,
                     0.7,
                     0.8,
                     0.62,
                     0.31,
                     0.99,
                     0.77,
                     0.95,
                     0.85,
                     0.97]
    nodes, edges = getGraph(predictedSentence, precitions, probabilities)
    print("Nur eine Relation pro Kante? {}".format(correctGraph(nodes, edges)))

    myPredictions = postProcessing([predictedSentence], [precitions], [probabilities])
    for p_set in myPredictions:
        print(p_set)

    nodes, edges = getGraph(predictedSentence, precitions, probabilities)
    print("Und jetzt? {}".format(correctGraph(nodes, edges)))
        

