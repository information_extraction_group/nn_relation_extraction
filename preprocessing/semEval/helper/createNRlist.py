# -*- coding: utf-8 -*-
import os

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
RESOURCES_FOLDER = os.path.join(module_location, '../../../resources/')
META_SPLIT       = ';;'

def createNormalReversedList(read, write):
    """Create a file that only contains, if a sample is reversed or not

    This will be important for calculating the F1 Score, since COMPARE won't be an option when the relation is reversed
    """

    with open(read, 'r', encoding='utf-8') as fIn:
        with open(write, 'w', encoding='utf-8') as fOut:
            for line in fIn:
                type = line.split(META_SPLIT)[1]
                fOut.write(type + '\n')

if __name__ == '__main__':
    source = RESOURCES_FOLDER + 'semEval/preprocessed/1.cropping.validation.txt'
    dest   = RESOURCES_FOLDER + 'semEval/preprocessed/corpora/1.validation.NR.txt'
    createNormalReversedList(source, dest)