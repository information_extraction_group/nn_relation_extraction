import random, os, ast

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
RESOURCES_FOLDER = os.path.join(module_location, '../resources/')

# 6 labels for semEval18 task 1 #
label2idx_semEval_1 = {}
with open(RESOURCES_FOLDER + 'semEval/labels2idx_task1.txt') as f:
    label2idx_semEval_1 = ast.literal_eval(f.read())

# 12 Labels for semEVal18 task 2 #
label2idx_semEval_2 = {}
with open(RESOURCES_FOLDER + 'semEval/labels2idx_task2.txt', 'r') as fIn:
    label2idx_semEval_2 = ast.literal_eval(fIn.read())

# 4 Labels for BioNLP #
label2idx_BioNLP = {}
with open(RESOURCES_FOLDER + 'BioNLP/labels2idx.txt', 'r') as fIn:
    label2idx_BioNLP = ast.literal_eval(fIn.read())

# 19 Labels for semEval10 #
label2idx_semEval2010 = {}
with open(RESOURCES_FOLDER + 'semEval2010/labels2idx.txt', 'r') as fIn:
    label2idx_semEval2010 = ast.literal_eval(fIn.read())

# 4 Labels for scienceIE #
label2idx_scienceIE = {}
with open(RESOURCES_FOLDER + 'scienceIE/labels2idx.txt', 'r') as fIn:
    label2idx_scienceIE = ast.literal_eval(fIn.read())