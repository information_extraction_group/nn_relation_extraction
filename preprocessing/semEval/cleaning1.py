# -*- coding: utf-8 -*-
import re
PUNCTUATION = '"#$%&\'()*,/=@[\\]^`{|}~'

def cleanLine(line):
    """Remove punctuation and reset . ! and ?

    Args:
        param1 (str): line that must be cleaned

    Returns:
        str: cleaned line
    
    """
    words = line.split()
    table = str.maketrans('', '', PUNCTUATION)
    stripped = [w.translate(table).strip() for w in words if w != '']
    line = ' '.join(stripped)
    return line.replace(" .", ".").replace(" ?", "?").replace(" !", "!").strip()

def useEntityTags(line):
    """Replace complex tags by <e>
    
    Args:
        param1 (str): input line
        
    Results:
        str: line with changed tag names
        
    """
    line = line.replace("</entity>", " <e>")
    line = re.sub(r"<entity id=\"[A-Z][0-9]*-[0-9]*.[0-9]*\">", "<e> ", line)
    return line


def clean(read, write):
    """Clean everything in the input file
    
    Args:
        param1 (str): Path to the input file
        param2 (str): Path to the destination file
        
    """
    with open(read, 'r', encoding='utf-8') as fIn:
        with open(write, 'w', encoding='utf-8') as fOut:
            for line in fIn:
                if line.strip() == '':
                    fOut.write('\n')
                    continue

                newLine = useEntityTags(line) 
                newLine = cleanLine(newLine)
                
                # Check if each line has at least 4 <e> tags before cleaning
                if newLine:
                    count = newLine.count("<e>")
                    assert(count >= 4)
                    newLine = ' '.join(newLine.split())
                    fOut.write(newLine + "\n")