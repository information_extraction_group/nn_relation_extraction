# -*- coding: utf-8 -*-
from pycorenlp import StanfordCoreNLP
from tqdm import tqdm

META_SPLIT = ';;'
corenlp_properties = {
    'annotators': 'tokenize, pos, ner',
    'outputFormat': 'json'
}

def hasNumbers(input):
    """Check if an input String has digits in it

    Args:
        param1 (str): input 

    Returns:
        boolean: Are there any digits?

    """
    return any(char.isdigit() for char in input)

def create_tags(line, corenlp):
    """Create POS-Tags for all words

    Replace numbers, that are not part of a proper noun by <NUM>
    POS-Tags for <e> are defined as EEE
    Server for StanfordNLP must be started: java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer

    Args:
        param1 (str): input line
        param2 (StanfordCoreNLP): tagger

    Returns:
        str: line with added POS-tag for each word

    """
    # Only get tags for sentences
    text = line.split(META_SPLIT)[-1].replace('.', '')
    meta = line.split(META_SPLIT)[:-1]

    newLine = ''

    corenlp_output = corenlp.annotate(text, properties=corenlp_properties).get('sentences', [])[0]
    for t in corenlp_output['tokens']:
        if(hasNumbers(t['originalText']) and ('NNP' not in t['pos'])):
            newLine += '<NUM>,NUM|'
        elif('<e>' in t['originalText']):
            newLine += '<e>,EEE|'
        elif(t['originalText'].strip() == '' and t['pos'].strip() == ''):
            newLine += ' ,NULL|'
        elif(t['originalText'].strip() == '' and t['pos'].strip()):
            newLine += ' ,' + t['pos'] + '|'
        else:
            # Check if the tag doesn't exist, give it the NULL type
            tag = t['pos']
            if tag == ',' or t['originalText'] == '|':
                continue
            newLine += t['originalText'] + ',' + tag + '|'

    l = newLine.strip('|')
    for p in l.split('|'):
        assert(len(p.split(',')) == 2)

    newLine = META_SPLIT.join(meta) + META_SPLIT + l
    return newLine

def createPOS(read, write):
    """Open StanfordCoreNLP server connection and tag words
    
    Args:
        param1 (str): input file path
        param2 (str): destination file path
        
    """
    corenlp = StanfordCoreNLP('http://localhost:9000')

    with open(read, 'r', encoding='utf-8') as fIn:
        with open(write, 'w', encoding='utf-8') as fOut:
            for line in tqdm(fIn):
                if line.strip() == '':
                    fOut.write('\n')
                    continue
                line = create_tags(line, corenlp)
                fOut.write(line + "\n")